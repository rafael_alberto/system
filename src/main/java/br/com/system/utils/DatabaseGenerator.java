package br.com.system.utils;

import java.io.File;
import java.util.EnumSet;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;

public class DatabaseGenerator {

	public static final String CONFIG_FILE = "src/main/resources/hibernate.cfg.xml";
	public static final String SCRIPT_FILE = "script.sql";

	public static void main(String[] args) {
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().configure(new File(CONFIG_FILE)).build();

		Metadata metadata = new MetadataSources(serviceRegistry).getMetadataBuilder().build();
		SchemaExport export = getSchemaExport();

		createDataBase(export, metadata);
		//dropDataBase(export, metadata);
	}

	private static SchemaExport getSchemaExport() {
		SchemaExport export = new SchemaExport();
		File outputFile = new File(SCRIPT_FILE);
		String outputFilePath = outputFile.getAbsolutePath();

		System.out.println("Export file: " + outputFilePath);

		export.setDelimiter(";");
		export.setOutputFile(outputFilePath);

		export.setHaltOnError(false);
		return export;
	}

	private static void createDataBase(SchemaExport export, Metadata metadata) {
		EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.DATABASE, TargetType.SCRIPT, TargetType.STDOUT);
		SchemaExport.Action action = SchemaExport.Action.CREATE;
		export.execute(targetTypes, action, metadata);
	}

	/*
	private static void dropDataBase(SchemaExport export, Metadata metadata) {
		EnumSet<TargetType> targetTypes = EnumSet.of(TargetType.DATABASE, TargetType.SCRIPT, TargetType.STDOUT);
		export.drop(targetTypes, metadata);
	}
	*/
}
