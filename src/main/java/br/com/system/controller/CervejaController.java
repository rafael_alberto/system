package br.com.system.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import br.com.system.exception.SystemException;
import br.com.system.model.Cerveja;
import br.com.system.service.CervejaService;

@Controller
@RequestMapping("/cerveja")
public class CervejaController {

	private static final String MODEL = "model";

	private static final String INDEX_CERVEJA = "cerveja/indexCerveja";
	private static final String FORM_CERVEJA = "cerveja/formCerveja";

	private static final String MENSAGEM = "mensagem";

	@Autowired
	private CervejaService cervejaService;
	
	@RequestMapping("/index")
	public ModelAndView index() {
		return new ModelAndView(INDEX_CERVEJA).addObject("cervejas", cervejaService.listarTodas());
	}
	
	@RequestMapping("/novo")
	public ModelAndView novo(@ModelAttribute(value = MODEL) Cerveja cerveja) {
		return new ModelAndView(FORM_CERVEJA).addObject(MODEL, cerveja);
	}

	@RequestMapping(value = "/salvar", method = RequestMethod.POST)
	public RedirectView salvar(@Valid Cerveja cerveja, BindingResult result, Model model,
			RedirectAttributes attributes) {
		RedirectView redirectView = new RedirectView(cerveja.getId() != null ? "editar?id=" + cerveja.getId() : "novo", true);
		attributes.addFlashAttribute(MODEL, cerveja);
		try {
			if (!result.hasErrors()) {
				cervejaService.salvar(cerveja);
				redirectView = new RedirectView("index", true);
				attributes.addFlashAttribute(MENSAGEM, "Cerveja salva com sucesso!");
			} else {
				cerveja.setUpdate(false);
				attributes.addFlashAttribute(MENSAGEM, result.getFieldError().getDefaultMessage());
			}			
		}catch(SystemException e) {
			cerveja.setUpdate(false);
			attributes.addFlashAttribute(MENSAGEM, e.getMessage());
		}
		return redirectView;
	}
	
	@RequestMapping(value = "/editar", method = RequestMethod.GET)
    public ModelAndView editar(@ModelAttribute(value = MODEL) Cerveja cerveja, 
    		@RequestParam(value = "id") Long id, HttpServletRequest request) {
        
        if(cerveja.isUpdate()){
        	cerveja = cervejaService.buscarPorId(id).get();
        }
       
        if (cerveja != null) {
            ModelAndView modelAndView = new ModelAndView(FORM_CERVEJA);
            modelAndView.addObject(MODEL, cerveja);
            return modelAndView;
        } else {
            ModelAndView modelAndView = index();
            return modelAndView;
        }
    }
	
	@RequestMapping(value = "/excluir", method = RequestMethod.GET)
    public RedirectView excluir(@RequestParam(value = "id") Long id,
    		HttpServletRequest request, RedirectAttributes redirectAttributes) {
    	try {
    		cervejaService.excluir(id);
    		redirectAttributes.addFlashAttribute(MENSAGEM, "Cerveja " + id + " excluída com sucesso!");
    	} catch (Exception e) {
    		redirectAttributes.addFlashAttribute(MENSAGEM, e.getMessage());
    	}
    	return new RedirectView("index", true);
    }
}
