package br.com.system.exception;

public class SystemException extends RuntimeException {

	private static final long serialVersionUID = 7111182092916866232L;

	public SystemException(String message) {
		super(message);
	}
}
