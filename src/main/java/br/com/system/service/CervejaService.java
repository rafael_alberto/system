package br.com.system.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.system.dao.CervejaDao;
import br.com.system.exception.SystemException;
import br.com.system.model.Cerveja;

@Service
public class CervejaService {

	@Autowired
	private CervejaDao cervejaDao;
	
	public List<Cerveja> listarTodas(){
		return cervejaDao.findAllByOrderByIdAsc();
	}
	
	public Optional<Cerveja> buscarPorId(Long id) {
		return cervejaDao.findById(id);
	}
	
	@Transactional
	public void salvar(Cerveja cerveja) {
		Optional<Cerveja> cervejaBD = cervejaDao.findBySku(cerveja.getSku());
		if((cervejaBD.isPresent() && cervejaBD.get().getId() != cerveja.getId())) {
			throw new SystemException("Cerveja com o SKU " + cerveja.getSku() + " já cadastrada!");			
		}
		cervejaDao.save(cerveja);
	}
	
	@Transactional
	public void excluir(Long id) {
		Optional<Cerveja> cerveja = buscarPorId(id);
		if(cerveja.isPresent()) {
			cervejaDao.delete(cerveja.get());
		}
	}
	
}
