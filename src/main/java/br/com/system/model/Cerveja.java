package br.com.system.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotBlank;

import br.com.system.annotation.SKU;

@Entity
@Table(name = "cervejas")
public class Cerveja implements Serializable {

	private static final long serialVersionUID = -7697624878911288036L;

	private static final String SEQUENCE_NAME = "seq_cervejas";

	@Id
	@GeneratedValue(generator = SEQUENCE_NAME)
	@SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME, allocationSize = 1)
	@Column(name = "id", nullable = false)
	private Long id;

	@SKU
	@NotBlank(message = "SKU é obrigatório")
	@Column(name = "sku", length = 8, nullable = false)
	private String sku;

	@NotBlank(message = "Nome é obrigatório")
	@Column(name = "nome", length = 15, nullable = false)
	private String nome;

	@Size(min = 5, max = 50, message = "Descrição deve possuir tamanho entre 5 e 50 caracteres")
	@Column(name = "descricao", length = 50, nullable = false)
	private String descricao;

	@Transient
	private boolean isUpdate = true;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSku() {
		return sku != null ? sku.toUpperCase() : sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isUpdate() {
		return isUpdate;
	}

	public void setUpdate(boolean isUpdate) {
		this.isUpdate = isUpdate;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Cerveja)) {
			return false;
		}

		Cerveja other = (Cerveja) obj;
		return new EqualsBuilder()
				.append(id, other.id)
				.append(sku, other.sku)
				.append(nome, other.nome)
				.append(descricao, other.descricao).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(id)
				.append(sku)
				.append(nome)
				.append(descricao).hashCode();
	}
}
