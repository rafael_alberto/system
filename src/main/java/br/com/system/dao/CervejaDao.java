package br.com.system.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.system.model.Cerveja;

@Repository
public interface CervejaDao extends JpaRepository<Cerveja, Long> {
	
	Optional<Cerveja> findBySku(String sku);
	Optional<Cerveja> findById(Long id);
	List<Cerveja> findAllByOrderByIdAsc();
}
