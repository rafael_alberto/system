<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<title><sitemesh:write property='title'/></title>

		<link rel="stylesheet" type="text/css" href="${context}/layout/bootstrap/atacama/dist/assets/stylesheets/vendors.min.css"/>
		<link rel="stylesheet" type="text/css" href="${context}/layout/bootstrap/atacama/dist/assets/stylesheets/algaworks.min.css"/>
		<link rel="stylesheet" type="text/css" href="${context}/layout/bootstrap/atacama/dist/assets/stylesheets/application.css"/>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<sitemesh:write property='head'/>
	</head>
	<body>
		<script type="text/javascript">
    		var context = "${context}";
		</script>
		
		<div class="aw-layout-page">
			<jsp:include page="/layout/includes/menu-top.jsp" />
			<jsp:include page="/layout/includes/menu-left.jsp" />
			<section class="aw-layout-content  js-content">
				<sitemesh:write property='body'/>	
			</section>	
		</div>

		<script src="${context}/layout/bootstrap/atacama/dist/assets/javascripts/vendors.min.js"></script>
		<script src="${context}/layout/bootstrap/atacama/dist/assets/javascripts/algaworks.min.js"></script>
		
	</body>
</html>