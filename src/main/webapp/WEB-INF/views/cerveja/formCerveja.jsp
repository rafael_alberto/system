<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cadastro de cervejas</title>
</head>
<body>
	<c:out value="${mensagem}"></c:out>
	<br>
	<form:form method="POST" modelAttribute="model" action="salvar">
		
		<c:if test="${model.id ne null}">
			<label>ID</label> 
			<form:input type="text" id="id" name="id" path="id" readonly="true"/> <br>
		</c:if>
		
		<label>SKU</label> 
		<form:input type="text" id="sku" name="sku" path="sku"/> <br> 
		<label>Nome</label>
		<form:input type="text" id="nome" name="nome" path="nome" /> <br>
		<label>Descrição</label>
		<form:input type="text" id="descricao" name="descricao" path="descricao" /> <br> 
		<input type="submit" value="salvar"><br>
		<a href="${context}/cerveja/index">Voltar</a>
	</form:form>
</body>
</html>