<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cadastro de cervejas</title>
</head>
<body>
	<div class="page-header">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-10">
					<h1>Pesquisa de produtos</h1>
				</div>

				<div class="col-xs-2">
					<div class="aw-page-header-controls">
						<a class="btn btn-primary" href="${context}/cerveja/novo"> <i
							class="fa  fa-plus-circle"></i> <span
							class="hidden-xs  hidden-sm">Novo produto</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="table-responsive">
			<table
				class="table  table-striped  table-bordered  table-hover  table-condensed  js-sticky-table">
				<thead>
					<tr>
						<th style="width: 10%;">ID</th>
						<th style="width: 10%;">SKU</th>
						<th style="width: 35%;">Nome</th>
						<th style="width: 35%;">Descrição</th>
						<th style="width: 5%;"></th>
						<th style="width: 5%;"></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${cervejas}" var="cerveja">
						<tr>
							<td>${cerveja.id}</td>
							<td>${cerveja.sku}</td>
							<td>${cerveja.nome}</td>
							<td>${cerveja.descricao}</td>
							<td style="text-align: center;">
								<a class="btn btn-default" style="width: 90px;" href="${context}/cerveja/editar?id=${cerveja.id}"> 
									<i class="fa  fa-pencil"> Editar</i>
								</a>
							</td>
							<td style="text-align: center;">
								<a class="btn btn-default" style="width: 90px;" href="${context}/cerveja/excluir?id=${cerveja.id}"> 
									<i class="fa  fa-trash"> Excluir</i>
								</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>